/*
*
  * STAFF SYSTEMS SAC
  * Copyright(c) 2009-2014, Lima Perú
  * Teléfono: (511) 664-7444
  * Email: soporte@staffsystems.pe
  * Website: http://www.staffsystems.pe
*
*/

$( document ).ready(function() {

    jQuery().staff_combo({
		id:	"carreras",
		renderer: "#demo",
		source : "_cursos.json"	
	});
		
	$("#carreras").change(function () {

		$("option:selected").each(function () {
			var value_select = $(this).val();
			
			if ($('#cursos').length==0){
			
				jQuery().staff_combo({
					id:	"cursos",
					renderer: "#demo",
					source : "_temas.json?=" + value_select
				});
				
			}		
			
		});
		
	});

});