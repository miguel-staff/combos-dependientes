function Create2DArray( var_rows ) {
  var arr = [];

  for (var i=0;i<var_rows;i++) {
     arr[i] = [];
  }

  return arr;
}
function get_datasource( var_webservice ){
		
	var arr;
	
		$.ajax({
			type: "GET",
			url: var_webservice,
			async:false, /*aca esta la magia*/
			success: function( response ){	
				var options = [];
				arr = Create2DArray( response.total );
				
				$.each(response.data, function( row, obj ) {	
					arr[row]["label"] = obj.label;
					arr[row]["value"] = obj.value;	
				});	
			},
			error: function (xhr, ajaxOptions, thrownError) { console.log( xhr ) ; }
		});
		
		return arr;
		
		
}
function build_combo( var_id, var_target, var_datasource ){

	var html= $("<select />",{
			id: var_id,
			name: var_id,
			change: function(){ 
				//console.log( "Handler for .change() called." ); 
			}
	}).append("<option value='null'>--Seleccione--</option>").appendTo( var_target );	

	for(var i=0; i<var_datasource.length;i++){
		
		$("<option />",{
			value : var_datasource[i]["value"],
			text: var_datasource[i]["label"]
		}).appendTo( html );
	}	
	
	//return html;
}



(function ($) {
    var methods = {
        
        init: function (options) {

            var settings = $.extend({
				id:	"combo",
				renderer: "body",
				source : ""			
			}, options);
  
		
			build_combo( 
				settings.id, // id
				$(settings.renderer), // target
				get_datasource(settings.source) //origen de datos
			);	

        }	
    };

    $.fn.staff_combo = function (method) {
        // Si existe la función la llamamos
        if (methods[method]) {
            return methods[method]
                .apply(this,
                    Array.prototype.slice.call(arguments, 1)
            );
        } else if (typeof method === 'object' || !method) {
            //Si no se pasa ningún parámetro o el parámetro es 
            //un objeto de configuración llamamos al inicializador  
            return methods.init.apply(this, arguments);
        } else {
            //En el resto de los casos mostramos un error
            $.error( 'El metodo: "' +method + '" no existe en el plugin "combos_dependientes"');
        }
    };
})(jQuery);

/*


    //INSTANCIANDO PLUGIN Y CARGANDO METODO POR DEFECTO
    $("body.home").staffpageplugin();
    
    //INSTANCIANDO PLUGIN Y ENVIANDO UN PARAMETRO AL METODO POR DEFECTO
    $("body.home").staffpageplugin({
        nombre:"Miguel Angel"
    });
    
    //INSTANCIANDO PLUGIN ESPECIFICANDO METODO A EJECUTAR, LUEGO ENVIANDO PARAMETROS
    $("body.home").staffpageplugin("pintar_elemento",{
        contenedor: $(".mod-bannertop"),
        background:"red"
    });

*/